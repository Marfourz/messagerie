const app = require("express")()

const http = require("http")

const server = http.createServer(app)

var io = require('socket.io')(server, { cors: { origin: "*" } });


app.get('/', (req, res) => {
    res.send("salut tout le monde")
});

io.on('connect', (socket) => {
    console.log("Une connexion")
})

io.on("newMessage", (socket) => {
    console.log("salut")
})

server.listen(3000, () => {
    console.log("Le serveur écoute sur le port 3000")
})